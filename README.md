# API (Crud Básico)

# How to run

## First Step: Install all packages

Run npm install --save-dev

## Second Step:

Create and configure the .env file with the credentials of the database. see the .env.example file to see how to do it.

## Third Step:

Run `npm run start` in the console or `nodemon start` if nodemon is installed

## Fourth Step:

Import the file Documentacion Sena.postman_collection.json to Postman or Insomnia and execute the API.

# Built With

- nodejs
- express
- mysql/postgres
