module.exports = (sequelize, type) => {
  const Campo = sequelize.define(
    "campo",
    {
      id: {
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        type: type.INTEGER,
      },
      nombre_campo: {
        allowNull: false,
        type: type.STRING(50),
      },
      tipo: {
        allowNull: false,
        type: type.STRING(50),
      },
      fk_formulario: {
        allowNull: false,
        type: type.INTEGER,
      },
    },
    {
      timestamps: true,
      freezeTableName: true,
      paranoid: true,
    }
  );

  Campo.afterCreate(async (campo) => await campo.reload());

  return Campo;
};
