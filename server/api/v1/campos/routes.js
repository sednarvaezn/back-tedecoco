const router = require("express").Router({
  mergeParams: true,
});

const controller = require("./controller");
const { auth } = require("../auth");

/*
 * /api/users/ POST - CREATE
 * /api/users/ GET - READ ALL
 * /api/users/:id GET - READ ONE
 * /api/users/:id PUT - UPDATE
 * /api/users/signin POST - SIGN IN IF USER IS ADMIN (ONLY FRONTEND)
 * /api/users/get-authorization POST - SIGN IN IF USER IS NOT ADMIN (ONLY EXTERNAL APPS)
 */

router.route("/:id").get(controller.getCampoById);

module.exports = router;
