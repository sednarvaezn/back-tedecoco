const { Campos: Model } = require("../../../sequelize");
const { Op } = require("sequelize");

exports.getCampoById = async (req, res, next) => {
  Model.findAll({ where: { fk_formulario: req.params.id } })
    .then((data) => {
      if (!!data) {
        res.status(200);
        res.json({
          status: "200",
          user: data,
        });
      } else {
        res.status(404);
        res.json({
          status: "404",
          user: "No hay Registros Activos con ese Id",
        });
      }
    })
    .catch((error) => {
      next(new Error(error));
    });
};
