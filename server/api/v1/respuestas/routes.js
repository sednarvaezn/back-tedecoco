const router = require("express").Router({
  mergeParams: true,
});

const controller = require("./controller");

router.route("/:form_id").get(controller.getAll);
router.route("/").post(controller.create);
module.exports = router;
