const dbo = require("../../../mongo");

exports.getAll = async function (req, res, next) {
  const dbConnect = dbo.getDb();
  dbConnect
    .collection("respuestas")
    .find({ form_id: parseInt(req.params.form_id) })
    .limit(50)
    .toArray(function (err, result) {
      if (err) {
        res.status(400).send("Error fetching listings!");
      } else {
        res.json(result);
      }
    });
};

exports.create = async function (req, res, next) {
  const dbConnect = dbo.getDb();
  const Document = {
    form_id: req.body.form_id,
    respuesta: req.body.respuesta,
  };

  dbConnect
    .collection("respuestas")
    .insertOne(Document, function (err, result) {
      if (err) {
        res.status(400).send("Error inserting answers!");
      } else {
        res.status(201).send({ status: 201, message: "Respuesta Guardada" });
      }
    });
};
