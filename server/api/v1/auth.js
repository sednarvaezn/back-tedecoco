const auth = (req, res, next) => {
  let token = req.headers.token;

  if (!token) {
    const message = "Unauthorized";
    next({
      success: false,
      message,
      statusCode: 401,
      level: "info",
    });
  }
};

module.exports = {
  auth,
};
