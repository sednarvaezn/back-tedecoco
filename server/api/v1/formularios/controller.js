const { Formularios: Model } = require("../../../sequelize");
const { Campos: CamposModel } = require("../../../sequelize");
const { Op } = require("sequelize");
var fs = require("fs");
const { forEach } = require("lodash");
xml2js = require("xml2js");
var parser = new xml2js.Parser();

function mapearDatos(datosJson) {
  datosJson = datosJson.mxfile.diagram[0].mxGraphModel[0].root[0].mxCell;

  validacionDiagrama = validarDiagrama(datosJson);
  var conceptos = [];
  if (validacionDiagrama[0]) {
    conceptos = buscarRelacionesTiene(datosJson);
  } else {
    return { message: validacionDiagrama[1] };
  }
  return conceptos;
}

function buscarRelacionesTiene(datos) {
  var conceptos = [];

  for (elemento of datos) {
    var style = elemento.$.style;
    var value = elemento.$.value;

    if (
      style != undefined &&
      style.indexOf("vsdxID=2") != -1 &&
      value.toLowerCase().indexOf(">tiene<") != -1
    ) {
      // Si el elemento es una relación estructural "tiene"
      var tieneId = elemento.$.id;
      var nombre_concepto_clase = "",
        nombre_conceptos_hoja = [];

      for (elemento2 of datos) {
        var source = elemento2.$.source;
        var target = elemento2.$.target;

        if (source != undefined && target != undefined) {
          // Si el elemento es una flecha
          if (target == tieneId) {
            nombre_concepto_clase = buscarNombreConID(source, datos);
          }
          if (source == tieneId) {
            var nombreAux = buscarNombreConID(target, datos).split(":");
            nombre_conceptos_hoja.push({
              nombre: nombreAux[0],
              tipoDeDato: nombreAux[1],
            });
          }
        }
      }
      conceptos.push({
        nombre: nombre_concepto_clase,
        atributos: nombre_conceptos_hoja,
      });
    }
  }

  return conceptos;
}

function buscarNombreConID(id, datos) {
  for (elemento of datos) {
    if (elemento.$.id == id) {
      return elemento.$.value.replace(/<\/?[\w\s\d;="-:#&]+>/g, "");
    }
  }
}

function validarDiagrama(datos) {
  // retorna un array con 2 valores, true o false en la primera posición y un texto informativo en la segunda posición
  var elementosConectados = [];
  var flechas = [];

  for (elemento of datos) {
    var style = elemento.$.style;
    var value = elemento.$.value;
    var source = elemento.$.source;
    var target = elemento.$.target;

    //console.log(elemento);
    if (
      (source == undefined || target == undefined) &&
      style != undefined &&
      (style.indexOf("vsdxID=57") != -1 ||
        style.indexOf(
          "edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;"
        ) != -1)
    ) {
      // Si alguna flecha está desconectada
      return [false, "Diagrama no válido, hay flechas desconectadas"];
    }

    if (
      style != undefined &&
      style.indexOf("vsdxID") == -1 &&
      style.indexOf(
        "edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;"
      ) == -1
    ) {
      // Si algún elemento externo al EP
      return [false, "Diagrama no válido, hay elementos externos al EP"];
    }

    if (
      value != undefined &&
      elemento.$.value.replace(/<\/?[\w\s\d;="-:#&]+>/g, "") == ""
    ) {
      // Si hay elementos vacíos
      return [false, "Diagrama no válido, hay elementos vacíos"];
    }

    if (
      style != undefined &&
      style.indexOf("vsdxID=2") != -1 &&
      value.toLowerCase().indexOf(">tiene<") == -1
    ) {
      // Si el elemento es una relación estructural diferente a "tiene"
      return [
        false,
        "Diagrama no válido, hay relaciones estructurales no soportadas",
      ];
    }

    if (
      style != undefined &&
      style.indexOf("vsdxID=1") != -1 &&
      value.toLowerCase().indexOf(">read<") == -1 &&
      value.toLowerCase().indexOf(">create<") == -1
    ) {
      // Si el elemento es una relación dinámica diferente a "Read" y "Create"
      return [
        false,
        "Diagrama no válido, hay relaciones dinámicas no soportadas",
      ];
    }

    if (source != undefined && target != undefined) {
      // guarda los id de elementos conectados y flechas
      elementosConectados.push(source);
      elementosConectados.push(target);
      flechas.push(elemento.$.id);
    }
  }

  for (elemento of datos) {
    var id = elemento.$.id;
    if (
      id != 0 &&
      id != 1 &&
      flechas.indexOf(id) == -1 &&
      elementosConectados.indexOf(id) == -1
    ) {
      return [false, "Diagrama no válido, hay elementos desconectados"];
    }
  }
  return [true, "Diagrama válido"];
}

exports.xml = async (req, res, next) => {
  fs.readFile(req.file.path, async function (err, data) {
    const promise = await new Promise((resolve, reject) => {
      parser.parseString(data, function (err, result) {
        resolve(mapearDatos(result));
      });
    });
    console.log("This is the promise", promise);
    if (!!promise.message) {
      return res.status(400).send({ message: promise.message });
    } else {
      let formulario = await Model.create({
        nombre_formulario: promise[0].nombre,
        fk_user: 1,
      });
      promise[0].atributos.forEach((atributo) => {
        CamposModel.create({
          nombre_campo: atributo.nombre,
          tipo: atributo.tipoDeDato,
          fk_formulario: formulario.dataValues.id,
        });
      });
      return res.status(200).send({ message: "File upload" });
    }
  });

  /*
  Model.findAll({ where: { fk_formulario: req.params.id } })
    .then((data) => {
      if (!!data) {
        res.status(200);
        res.json({
          status: "SL200",
          user: data,
        });
      } else {
        res.status(404);
        res.json({
          status: "404",
          user: "No hay Registros Activos con ese Id",
        });
      }
    })
    .catch((error) => {
      next(new Error(error));
    });*/
};
exports.getAllbyId = async (req, res, next) => {
  Model.findAll({ where: { fk_user: req.params.id } })
    .then((data) => {
      if (!!data) {
        res.status(200);
        res.json({
          status: "200",
          data: data,
        });
      } else {
        res.status(404);
        res.json({
          status: "404",
          data: "No hay Registros Activos con ese Id",
        });
      }
    })
    .catch((error) => {
      next(new Error(error));
    });
};
