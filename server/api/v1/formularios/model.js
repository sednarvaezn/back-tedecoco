module.exports = (sequelize, type) => {
  const Formulario = sequelize.define(
    "formulario",
    {
      id: {
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        type: type.INTEGER,
      },
      nombre_formulario: {
        allowNull: false,
        type: type.STRING(50),
        unique: true,
      },
      fk_user: {
        allowNull: false,
        type: type.INTEGER,
      },
    },
    {
      timestamps: true,
      freezeTableName: true,
      paranoid: true,
    }
  );

  Formulario.afterCreate(async (formulario) => await formulario.reload());

  return Formulario;
};
