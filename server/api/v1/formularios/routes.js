const router = require("express").Router({
  mergeParams: true,
});

const controller = require("./controller");
const { auth } = require("../auth");
var multer = require("multer");
var path = require("path");
/*
 * /api/users/ POST - CREATE
 * /api/users/ GET - READ ALL
 * /api/users/:id GET - READ ONE
 * /api/users/:id PUT - UPDATE
 * /api/users/signin POST - SIGN IN IF USER IS ADMIN (ONLY FRONTEND)
 * /api/users/get-authorization POST - SIGN IN IF USER IS NOT ADMIN (ONLY EXTERNAL APPS)
 */
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(__dirname, "../../../files"));
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + path.extname(file.originalname)); //Appending extension
  },
});
var upload = multer({ storage: storage });

router.route("/").post(upload.single("file"), controller.xml);
router.route("/:id").get(controller.getAllbyId);

module.exports = router;
