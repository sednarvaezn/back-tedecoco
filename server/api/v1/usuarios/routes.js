const router = require("express").Router({
  mergeParams: true,
});

const controller = require("./controller");
const { auth } = require("../auth");

/*
 * /api/users/ POST - CREATE
 * /api/users/ GET - READ ALL
 * /api/users/:id GET - READ ONE
 * /api/users/:id PUT - UPDATE
 * /api/users/signin POST - SIGN IN IF USER IS ADMIN (ONLY FRONTEND)
 * /api/users/get-authorization POST - SIGN IN IF USER IS NOT ADMIN (ONLY EXTERNAL APPS)
 */

router.route("/login").post(controller.login);
router.route("/desloguear").post(controller.logout);

router.route("/desactived").get(auth, controller.getAllUsersDesactivated);
router.route("/enable").post(auth, controller.enable);

router
  .route("/")
  .get(auth, controller.getAllUsers)
  .post(auth, controller.create);

router
  .route("/:id")
  .get(auth, controller.getUserById)
  .put(auth, controller.update)
  .delete(auth, controller.delete);

module.exports = router;
