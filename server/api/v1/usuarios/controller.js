const { Users: Model } = require("../../../sequelize");
const { Op } = require("sequelize");

exports.login = async (req, res, next) => {
  const { username, password } = req.body;
  const user = await autenticar(username, password);
  if (!!user) {
    res.status(200);
    res.json({
      status: 1,
      user: {
        id: user.id,
        firstname: user.firstname,
        lastname: user.lastname,
        assignedboss: user.assignedboss,
        idCustomInfo: user.idCustomInfo,
        idProfile: user.idProfile,
        createdAt: user.createdAt,
        updateAt: user.updateAt,
        deleteAt: user.deleteAt,
        token: token,
      },
    });
  } else {
    /*Excepcion cuando no se encuentra usuario*/
  }
};

const autenticar = async (username, password) => {
  const data = await Model.findOne({ where: { username } });
  if (!!data) {
    if (validarContrasena(password, data.password)) {
      return data;
    } else {
      return false;
    }
  }
};

validarContrasena = async (password, dbpassword) => {
  if (dbpassword == password) {
    return true;
  } else {
    return false;
  }
};

exports.logout = async (req, res, next) => {
  res.status(200);
  res.json({
    status: 1,
    close: "Session Cerrada",
  });
};

exports.create = async (req, res, next) => {
  const { body = {} } = req;

  Model.create(body)
    .then((data) => {
      res.status(201);
      res.json({
        status: "SL201",
        message: "Usuario creado",
        id: data.id,
      });
    })
    .catch((error) => {
      next(new Error(error));
    });
};

exports.getAllUsersDesactivated = async (req, res, next) => {
  Model.findAll({
    where: { deleteAt: { [Op.ne]: null } },
    paranoid: false,
  })
    .then((data) => {
      res.status(200);
      res.json({
        status: "SL200",
        users: data,
      });
    })
    .catch((error) => {
      next(new Error(error));
    });
};

exports.getUserById = async (req, res, next) => {
  Model.findByPk(req.params.id)
    .then((data) => {
      if (!!data) {
        res.status(200);
        res.json({
          status: "SL200",
          user: data,
        });
      } else {
        res.status(425);
        res.json({
          status: "SL425",
          user: "No hay Registros Activos con ese Id",
        });
      }
    })
    .catch((error) => {
      next(new Error(error));
    });
};

exports.getAllUsers = async (req, res, next) => {
  Model.findAll()
    .then((data) => {
      res.status(200);
      res.json({
        status: "SL200",
        users: data,
      });
    })
    .catch((error) => {
      next(new Error(error));
    });
};

exports.update = (req, res, next) => {
  const { body } = req;

  Model.update(body, { where: { id: req.params.id } })
    .then(() => {
      res.status(200);
      res.json({
        status: "SL200",
        message: "Registro Actualizado correctamente",
      });
    })
    .catch((error) => {
      next(new Error(error));
    });
};

exports.enable = async (req, res, next) => {
  const { id } = req.body;
  Model.restore({ where: { id } })
    .then(() => {
      res.status(200);
      res.json({
        status: "SL200",
        users: "Registro Activado",
      });
    })
    .catch((error) => {
      next(new Error(error));
    });
};

exports.delete = async (req, res, next) => {
  const id = req.params.id;
  Model.destroy({ where: { id } })
    .then(() => {
      res.status(200);
      res.json({
        status: "SL200",
        users: "Registro Eliminado correctamente",
      });
    })
    .catch((error) => {
      next(new Error(error));
    });
};
