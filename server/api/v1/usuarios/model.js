//const { hash, compare } = require("bcryptjs");
const { md5 } = require("md5");
module.exports = (sequelize, type) => {
  const User = sequelize.define(
    "user",
    {
      id: {
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        type: type.INTEGER,
      },
      firstname: {
        type: type.STRING(160),
        allowNull: false,
      },
      lastname: {
        type: type.STRING(160),
        allowNull: false,
      },
      username: {
        type: type.STRING(160),
        unique: true,
        allowNull: false,
      },
      password: {
        type: type.STRING(160),
        allowNull: false,
      },
    },
    {
      timestamps: true,
      freezeTableName: true,
      paranoid: true,
    }
  );
  async function encryptPasswordIfChanged(user) {
    if (user.changed("password")) {
      user.password = await md5(user.password);
    }
  }

  User.afterCreate(async (user) => await user.reload());
  User.beforeCreate(encryptPasswordIfChanged);
  User.beforeUpdate(encryptPasswordIfChanged);
  User.prototype.comparePassword = async function comparePassword(password) {
    return compare(password, this.password);
  };

  return User;
};
