const router = require("express").Router();
//Import the routes of the models//
const users = require("./usuarios/routes");
const campos = require("./campos/routes");
const formulario = require("./formularios/routes");
const respuestas = require("./respuestas/routes");
//Routing//
router.use("/users", users);
router.use("/campos", campos);
router.use("/formulario", formulario);
router.use("/respuestas", respuestas);
module.exports = router;
