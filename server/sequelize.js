const Sequelize = require("sequelize");
const UserModel = require("./api/v1/usuarios/model");
const FormulariosModel = require("./api/v1/formularios/model");
const CamposModel = require("./api/v1/campos/model");

const config = require("./config");
const logger = require("./config/logger");
const bodyParser = require("body-parser");
// Database Config//
const { dbname, username, password, host, dialect } = config.database;

const sequelize = new Sequelize(dbname, username, password, {
  host,
  port: 5432,
  dialect,
  pool: {
    max: 10,
    min: 0,
    require: 30000,
    idle: 10000,
  },
  logging: false,
  timezone: "-05:00",
  dialectOptions: {
    ssl: {
      require: true,
      rejectUnauthorized: false,
    },
  },
});

// database object with the models//
const db = {
  Users: UserModel(sequelize, Sequelize),
  Campos: CamposModel(sequelize, Sequelize),

  Formularios: FormulariosModel(sequelize, Sequelize),
};

// Model relations
db.Users.hasMany(db.Formularios, { foreignKey: "fk_user", sourceKey: "id" });
db.Formularios.belongsTo(db.Users, { foreignKey: "fk_user", sourceKey: "id" });
db.Formularios.hasMany(db.Campos, {
  foreignKey: "fk_formulario",
  sourceKey: "id",
});
db.Campos.belongsTo(db.Formularios, {
  foreignKey: "fk_formulario",
  sourceKey: "id",
});

// Load relations of models
Object.keys(db).forEach((modelName) => {
  if ("associate" in db[modelName]) {
    db[modelName].associate(db);
  }
});

sequelize
  .authenticate()
  .then(() => {
    logger.info("Database connected.");
  })
  .catch((err) => {
    logger.error("Database not connected.");
  });

sequelize.sync().then(() => {
  logger.info("Synchronized database.");
});

module.exports = { ...db };
