require("dotenv").config("");

const config = {
  server: {
    port: process.env.SERVER_PORT || 4001,
  },
  database: {
    dbname: process.env.DATABASE_DBNAME,
    username: process.env.DATABASE_USERNAME,
    password: process.env.DATABASE_PASSWORD,
    host: process.env.DATABASE_HOST,
    dialect: "postgres",
  },
  mongodatabase: {
    uri: process.env.MONGO_URI,
  },
};

module.exports = config;
