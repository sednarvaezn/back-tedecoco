const http = require("http");

const app = require("./server");
const config = require("./server/config");
const dbo = require("./server/mongo");

const { port } = config.server;

dbo.connectToServer(function (err) {
  if (err) {
    console.error(err);
    process.exit();
  }
  const server = http.createServer(app);
  server.listen(port, () => {
    console.log(`Server running at port: ${port}`);
  });
});
